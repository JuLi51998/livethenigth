import { Router } from "express";
import { innerController } from "../controllers/innerController";

class InnerRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }

    config(): void {
        this.router.get('/', innerController.datosUsuario);
        this.router.post('/:id', innerController.datosUsuario);
        this.router.put('/:id', innerController.updateData);
    }
}

const innerRoutes = new InnerRoutes();
export default innerRoutes.router;