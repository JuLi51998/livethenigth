import { Router } from "express";
import { indexController } from '../controllers/indexController'
import { auth } from "../middlewares/auth";

class IndexRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }
    
    config(): void {
        this.router.get('/', indexController.index)
        this.router.get('/login', indexController.login);
        this.router.get('/registro', indexController.registrar);

        this.router.post('/', indexController.index);
        this.router.post('/login', indexController.login);
        this.router.post('/registro', indexController.registrar);
        this.router.get('/privada', auth.isAuth, function(req: any, res: any) {
            res.status(200).send({message: 'Tienes accesso'})
        })
    }
}

const indexRoutes =  new IndexRoutes();
export default  indexRoutes.router;