import express, { Application } from "express";
import morgan from "morgan";
import cors from "cors";

//Rutas
import indexRoutes from "./routes/indexRoutes";
import innerRoutes from "./routes/innerRoutes";

class backend {

    public app: Application;
    
    constructor() {
        this.app = express();
        this.config();
        this.routes();
    }

    config(): void {
        this.app.set('port', process.env.port || 3000);
        this.app.use(morgan('dev')); //ver que peticiones hace el usuario
        this.app.use(cors()); //pedir datos a servidor
        this.app.use(express.json()); //el servidor entiende los json
        this.app.use(express.urlencoded({extended: false})); //enviar de un formulario html
    }

    routes(): void {
        this.app.use(indexRoutes);
        this.app.use('/inner', innerRoutes)
    }

    start(): void {
        this.app.listen(this.app.get('port'), () => {
            console.log('Server on port ', this.app.get('port'));
        })
    }
}

const serve = new backend();
serve.start();