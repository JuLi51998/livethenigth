import jwt from 'jwt-simple'
import moment from 'moment'

class Auth {
    isAuth(req: any, res: any, next: any) {
        if(!req.headers.autorization) {
            return res.status(403).send({message: "No tienes autorizacion"});
        }
        const token = req.headers.autorization.split(" ")[1];  //Token que ha enviado el cliente en la cabecera
        const payload = jwt.decode(token, "clavecontokens");

        if(payload.exp <= moment().unix()){
            return res.status(401).send({message: "EL token ha expirado"});
        }

        req.user = payload.sub;
        next();
    }
}

export const auth = new Auth;