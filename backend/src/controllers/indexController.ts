import { Request, Response } from "express";
import pool from "../database";
import { indexModel } from "../modelos/indexModel";
import { service } from "../services/index.service";

class IndexController {

    public async index (req: Request, res:Response){
        const users = await indexModel.getUsuarios();
        res.json(users);
    }

    public async login (req: Request, res:Response) {
        //res.json({text: 'API esta en /inner'})
        await indexModel
    }

    public async registrar(req: Request, res:Response) {
        //console.log("Hola", req);
        await indexModel.createUser(req.body);
        res.json({user: req.body,token: service.createToken(req.body)})
    }
}

export const indexController = new IndexController();