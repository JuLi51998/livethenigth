import { Request, Response } from "express";
import pool from "../database";

class InnerController {

    public async datosUsuario(req: Request, res:Response): Promise<any> {
        const { id } = req.params;
        const userData = await pool.query('SELECT * FROM users WHERE idUser = ?', [id])
        if(userData.length > 0) {
            res.json(userData[0]);
        }
        res.status(404).json({message: 'El usuario no existe'});
    }

    public async updateData(req:Request, res:Response):Promise<void> {
        const { id } = req.params;
        await pool.query('UPDATE users SET ? WHERE idUser = ?', [req.body, id])
        res.json({message: 'Los datos del usuario han sido actualizados'});
    }
}

export const innerController = new InnerController();