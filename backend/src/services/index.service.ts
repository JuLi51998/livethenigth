import jws from 'jwt-simple'
import moment from "moment";

class IndexService {
    createToken(user: any) {
        const payload = {
            sub: user._id,
            iat: moment().unix(),
            exp: moment().add(14, 'days').unix(),
        }
        return jws.encode(payload, 'clavecontokens')
    }
}

export const service = new IndexService();
