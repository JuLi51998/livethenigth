"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const indexModel_1 = require("../modelos/indexModel");
const index_service_1 = require("../services/index.service");
class IndexController {
    index(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const users = yield indexModel_1.indexModel.getUsuarios();
            res.json(users);
        });
    }
    login(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            //res.json({text: 'API esta en /inner'})
            yield indexModel_1.indexModel;
        });
    }
    registrar(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            //console.log("Hola", req);
            yield indexModel_1.indexModel.createUser(req.body);
            res.json({ user: req.body, token: index_service_1.service.createToken(req.body) });
        });
    }
}
exports.indexController = new IndexController();
