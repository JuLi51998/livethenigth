"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jwt_simple_1 = __importDefault(require("jwt-simple"));
const moment_1 = __importDefault(require("moment"));
class Auth {
    isAuth(req, res, next) {
        if (!req.headers.autorization) {
            return res.status(403).send({ message: "No tienes autorizacion" });
        }
        const token = req.headers.autorization.split(" ")[1]; //Token que ha enviado el cliente en la cabecera
        const payload = jwt_simple_1.default.decode(token, "clavecontokens");
        if (payload.exp <= moment_1.default().unix()) {
            return res.status(401).send({ message: "EL token ha expirado" });
        }
        req.user = payload.sub;
        next();
    }
}
exports.auth = new Auth;
