"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const innerController_1 = require("../controllers/innerController");
class InnerRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/', innerController_1.innerController.datosUsuario);
        this.router.post('/:id', innerController_1.innerController.datosUsuario);
        this.router.put('/:id', innerController_1.innerController.updateData);
    }
}
const innerRoutes = new InnerRoutes();
exports.default = innerRoutes.router;
