"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const indexController_1 = require("../controllers/indexController");
const auth_1 = require("../middlewares/auth");
class IndexRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/', indexController_1.indexController.index);
        this.router.get('/login', indexController_1.indexController.login);
        this.router.get('/registro', indexController_1.indexController.registrar);
        this.router.post('/', indexController_1.indexController.index);
        this.router.post('/login', indexController_1.indexController.login);
        this.router.post('/registro', indexController_1.indexController.registrar);
        this.router.get('/privada', auth_1.auth.isAuth, function (req, res) {
            res.status(200).send({ message: 'Tienes accesso' });
        });
    }
}
const indexRoutes = new IndexRoutes();
exports.default = indexRoutes.router;
