"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const cors_1 = __importDefault(require("cors"));
//Rutas
const indexRoutes_1 = __importDefault(require("./routes/indexRoutes"));
const innerRoutes_1 = __importDefault(require("./routes/innerRoutes"));
class backend {
    constructor() {
        this.app = express_1.default();
        this.config();
        this.routes();
    }
    config() {
        this.app.set('port', process.env.port || 3000);
        this.app.use(morgan_1.default('dev')); //ver que peticiones hace el usuario
        this.app.use(cors_1.default()); //pedir datos a servidor
        this.app.use(express_1.default.json()); //el servidor entiende los json
        this.app.use(express_1.default.urlencoded({ extended: false })); //enviar de un formulario html
    }
    routes() {
        this.app.use(indexRoutes_1.default);
        this.app.use('/inner', innerRoutes_1.default);
    }
    start() {
        this.app.listen(this.app.get('port'), () => {
            console.log('Server on port ', this.app.get('port'));
        });
    }
}
const serve = new backend();
serve.start();
