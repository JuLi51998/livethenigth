CREATE DATABASE livethenigthdb;

USE livethenigthdb;

/*CREATE TABLE `usuarios` (
  `id_user` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `nombre` VARCHAR(50) NOT NULL,
  `password` VARCHAR(50) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `modo` VARCHAR(100) NOT NULL,
  `foto` VARCHAR(100) NOT NULL,
  `verificacion` VARCHAR(50) NOT NULL,
  `emailEncriptado` VARCHAR(100)  NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
);*/

/*DESCRIBE usuarios;*/

--
-- Table structure for table `acctiondummytable`
--

CREATE TABLE `acctiondummytable` (
  `idAcction` int(11) NOT NULL,
  `NameAcction` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gengerdummytable`
--

CREATE TABLE `gengerdummytable` (
  `idGender` int(11) NOT NULL,
  `NameGender` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `matchaccept`
--

CREATE TABLE `matchaccept` (
  `idMatchAccept` int(11) NOT NULL COMMENT 'Id de los emparejamientos mutuos entre usuarios.',
  `idUser` int(11) NOT NULL,
  `UserLikeUsers` varchar(500) NOT NULL COMMENT 'Lista de usuarios que le interesan a el usuario.',
  `UsersLikeUser` varchar(500) NOT NULL COMMENT 'Lista de usuarios que están interesados en el usuario usuario.',
  `CompleteMatch` varchar(255) NOT NULL COMMENT '  con dónde el interés fue mutuo.'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `matchrejected`
--

CREATE TABLE `matchrejected` (
  `idMatchRejected` int(11) NOT NULL COMMENT 'Id de los emparejamientos rechazados por el usuario.',
  `idUser` int(11) NOT NULL COMMENT 'Id del usuario principal.',
  `UsersRejectxUser` varchar(255) NOT NULL COMMENT 'Usuarios rechazados por el usuario.'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `provisionallogs`
--

CREATE TABLE `provisionallogs` (
  `idLog` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `currentAcction` varchar(500) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `idUser` int(11) NOT NULL,
  `UserName` text COLLATE utf8_spanish_ci NOT NULL,
  `UserPassword` text COLLATE utf8_spanish_ci NOT NULL,
  `UserEmail` text COLLATE utf8_spanish_ci NOT NULL,
  `RegisterMode` text COLLATE utf8_spanish_ci NOT NULL,
  `UserPhoto` text COLLATE utf8_spanish_ci NOT NULL,
  `UserAge` int(3) NOT NULL,
  `UserGender` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `InterestGender` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `PlaceWork` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PlaceStudy` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `UserVerification` int(11) NOT NULL,
  ` EncryptedEmail` text COLLATE utf8_spanish_ci NOT NULL,
  `Date` timestamp NOT NULL DEFAULT current_timestamp(),
  `UserIsActive` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acctiondummytable`
--
ALTER TABLE `acctiondummytable`
  ADD PRIMARY KEY (`idAcction`);

--
-- Indexes for table `gengerdummytable`
--
ALTER TABLE `gengerdummytable`
  ADD PRIMARY KEY (`idGender`);

--
-- Indexes for table `matchaccept`
--
ALTER TABLE `matchaccept`
  ADD PRIMARY KEY (`idMatchAccept`),
  ADD KEY `ForeingKeyAccept` (`idUser`);

--
-- Indexes for table `matchrejected`
--
ALTER TABLE `matchrejected`
  ADD PRIMARY KEY (`idMatchRejected`),
  ADD KEY `ForeingKeyxTBLUser` (`idUser`);

--
-- Indexes for table `provisionallogs`
--
ALTER TABLE `provisionallogs`
  ADD PRIMARY KEY (`idLog`),
  ADD KEY `ForeingKeyLogs` (`idUser`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idUser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acctiondummytable`
--
ALTER TABLE `acctiondummytable`
  MODIFY `idAcction` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gengerdummytable`
--
ALTER TABLE `gengerdummytable`
  MODIFY `idGender` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `matchaccept`
--
ALTER TABLE `matchaccept`
  MODIFY `idMatchAccept` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id de los emparejamientos mutuos entre usuarios.';

--
-- AUTO_INCREMENT for table `matchrejected`
--
ALTER TABLE `matchrejected`
  MODIFY `idMatchRejected` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id de los emparejamientos rechazados por el usuario.';

--
-- AUTO_INCREMENT for table `provisionallogs`
--
ALTER TABLE `provisionallogs`
  MODIFY `idLog` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `matchaccept`
--
ALTER TABLE `matchaccept`
  ADD CONSTRAINT `ForeingKeyAccept` FOREIGN KEY (`idUser`) REFERENCES `users` (`idUser`);

--
-- Constraints for table `matchrejected`
--
ALTER TABLE `matchrejected`
  ADD CONSTRAINT `ForeingKeyxTBLUser` FOREIGN KEY (`idUser`) REFERENCES `users` (`idUser`) ON DELETE CASCADE;

--
-- Constraints for table `provisionallogs`
--
ALTER TABLE `provisionallogs`
  ADD CONSTRAINT `ForeingKeyLogs` FOREIGN KEY (`idUser`) REFERENCES `users` (`idUser`) ON DELETE CASCADE;

ALTER TABLE `livethenigthdb`.`users` 
  CHANGE COLUMN ` EncryptedEmail` `EncryptedEmail` TEXT NOT NULL ;

ALTER TABLE `livethenigthdb`.`users` 
  ADD COLUMN `BirthDate` DATE NULL AFTER `UserIsActive`;


COMMIT;